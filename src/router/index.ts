import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Auth from '@/views/Auth.vue';
import Register from '@/views/Register.vue';
import Profile from '@/views/Profile.vue';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      protected: true,
      title: 'Сводка'
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    meta: {
      protected: true,
      title: 'Тестовая'
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta: {
      protected: true,
      title: 'Профиль'
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      protected: false
    },
    beforeEnter(to, from, next) {
      if (localStorage.getItem('token')) {
        next('/')
      } else {
        next()
      }
    }
  },
  {
    path: '/auth',
    name: 'Auth',
    component: Auth,
    meta: {
      protected: false
    },
    beforeEnter(to, from, next) {
      if(localStorage.getItem('token')) {
        next('/')
      } else {
        next()
      }
    }
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if(!localStorage.getItem('token') && to.meta.protected) {
    next('/auth')
  } else {
    next()
  }
});

export default router
