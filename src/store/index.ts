import Vue from 'vue'
import Vuex from 'vuex'
import {env} from '@/environment';
import router from '@/router';

Vue.use(Vuex)

interface Login {
  username: string;
  passwd_hash: string;
  password: string;
}

interface Register extends Login {
  email: string;
  state: number;
}

export default new Vuex.Store({
  state: {
    scrolledHeader: false,
    token: localStorage.getItem('token') || "",
    username: localStorage.getItem('username') || "",
    error: {
      message: "",
      isShown: false
    }
  },
  mutations: {
    setScrolledHeader(state, payload: boolean) {
      console.log(payload)
      return state.scrolledHeader = payload;
    },
    setToken(state, payload: string) {
      return state.token = payload;
    },
    setUsername(state, payload: string) {
      return state.username = payload;
    }
  },
  actions: {
    login(context, payload: Login) {
      fetch(env.api + '/auth', {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        }
      }).then(res => res.json()).then(data => {if(data.token){
        localStorage.setItem('username', payload.username);
        context.commit('setUsername', payload.username);
        localStorage.setItem('token', data.token);
        context.commit('setToken', data.token);
        router.push('/')
      }});
      return context.commit('setUsername', payload.username);
    },
    register(context, payload: Register) {
      fetch(env.api + '/register', {
        method: 'POST',
        // body: JSON.stringify({
        //   username: payload.username,
        //   passwd_hash: payload.password,
        //   email: payload.email,
        //   state: payload.state
        // }),
        body: `{"username": "${payload.username}","passwd_hash": "${payload.password}","email": "${payload.email}","state": ${payload.state}}`,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        }
      }).then(res => res.text()).then(data => console.log(data))
    },
    logout(context) {
      context.state.username = "";
      context.state.token = "";
      localStorage.removeItem('token');
      localStorage.removeItem('username');
      router.push('/')
    },
    showError(context, payload: string) {
      context.state.error.message = payload;
      context.state.error.isShown = true;
      setTimeout(() => {
        context.state.error.isShown = false;
      }, 5000)
    }
  },
  modules: {
  }
})
